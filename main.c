
#include <stdio.h>
#include <time.h>

#ifndef __linux__
#include <windows.h>
#else
#include <pthread.h>
#endif


#ifdef __linux__
#define Sleep(a) usleep(a*1000)
#endif

#ifndef __linux__
HANDLE hStdOut;
#endif // __linux__


int M =171;
int N = 50;
char** matrix;
char** mask;
void render(char**matrix, char** mask, int n, int m);
void make(char** matrix, char** mask, int n, int m);
void crash(char** matrix, char** mask, int n, int m, int is, int ie, int j);
char chF[14][15];
void  crRectange(char**arr, int ix, int jx, int is, int ij){
	int i, j;
	for(i = 0; i < is; i++){
		for(j = 0; j < ij; j++){
			arr[ix+i][jx+j] = -1;
		}
	}
}
void wr_chF(char**arr, int ix, int jx){
	int i, j;
	crRectange(arr, ix, jx, 28, 6);
	crRectange(arr, ix, jx, 4, 30);
	crRectange(arr, ix+12, jx, 4, 16);
}
void wr_chI(char**arr, int ix, int jx){
	int i, j;
	crRectange(arr, ix, jx+12, 28, 6);
	crRectange(arr, ix, jx+6, 4, 18);
	crRectange(arr, ix+24, jx+6, 4, 18);
	//crRectange(arr, ix+12, jx, 4, 16);
}
void wr_chT(char**arr, int ix, int jx){
	int i, j;
	crRectange(arr, ix, jx+12, 28, 6);
	crRectange(arr, ix, jx, 4, 30);
	//crRectange(arr, ix+24, jx+6, 4, 18);
	//crRectange(arr, ix+12, jx, 4, 16);
}
#ifndef __linux__
DWORD WINAPI ThreadFunc1(void* vptr_args) {
#else
void thread_func1(void *vptr_args) {
#endif
  	while(1){
		render(matrix, mask, N, M);
		Sleep(30);
	}
  return 0;
}
#ifndef __linux__
DWORD WINAPI ThreadFunc2(void* vptr_args) {
#else
void thread_func2(void *vptr_args) {
#endif
  	while(1){
		make(matrix, mask, N, M);
	}
  return 0;
}
#ifndef __linux__
DWORD WINAPI ThreadFunc3(void* vptr_args) {
#else
void thread_func3(void *vptr_args) {
#endif
  	int* a = (int*) vptr_args;
	//Sleep(2000);
	memset(mask[3]+20, 2, 30);
	memset(matrix[3]+20, 0, 30);
	sprintf(matrix[3]+20, "%d %d %d",a[0], a[1], a[2]);
	crash(matrix, mask, N, M, a[0], a[1], a[2]);
	free(vptr_args);
	pthread_exit(vptr_args);

  return 0;
}

int main(int argc, char *argv[])
{
	srand(time(NULL));
#ifndef __linux__
	hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
#endif // __linux__
	matrix = calloc(N, sizeof(char*));
	mask = calloc(N, sizeof(char*));
	int i;
	for(i = 0; i < N; i++){
		matrix[i] = calloc(M, sizeof(char));
	}
	for(i = 0; i < N; i++){
		mask[i] = calloc(M, sizeof(char));
	}
	wr_chF(mask, 16,  20);
	wr_chI(mask, 16,  20 + 34 + 20);
	wr_chT(mask, 16,  20 + 34 + 20 +34+20);
#ifndef __linux__
	//printf("\033[;h;wt\n");
	HANDLE thread1 = CreateThread(NULL, 0, ThreadFunc1, NULL, 0, NULL);
	for(i = 0; i < 50; i++){
		CreateThread(NULL, 0, ThreadFunc2, NULL, 0, NULL);
	}
#else
	pthread_t thread1;
	pthread_create(&thread1, NULL, thread_func1, NULL);
	for(i = 0; i < 100; i++){
		pthread_create(&thread1, NULL, thread_func2, NULL);
//	printf("\033[%d;%dH", 10, 10);
//	printf("Hello, world!\n");
//	Sleep(1000);
//	printf("\033[%d;%dH", i, i);
	//printf("             ");
	}
#endif // __linux__
	getchar();
	printf("\033[0m\n");
	system("clear");

	 return 0;
}

void make(char** matrix, char** mask, int n, int m){
	int i, j,k, ix, id, ie,is,ke;
	j = rand()%m;

	ie = rand()%n;
	is = rand()%n;
	if(is>ie){
		ie+=n+1;
	}
	for(i = is; i <= ie; i++){
		if(mask[i%n][j]>=0)
			if((j%2)/*&&((i%n)%2)*/){
				ke = rand()%5;
				for(k=0;k<ke;k++){
					matrix[i%n][j] = (rand()%26);
					if(matrix[i%n][j]<16)
						matrix[i%n][j]+=65;
					else
						matrix[i%n][j]+=48-16;
					//+65;
					mask[i%n][j] =k%2+1;
					Sleep(50);
				}
				if(i==ie||i==ie-1)
					mask[i%n][j] = 1;
				else
					mask[i%n][j] = 1;//rand()%2+1;
				//Sleep(500);
			}
	}
	int *a = (int*) calloc(3, sizeof(int));
	a[0]=is;
	a[1]=ie;
	a[2]=j;
	memset(mask[4]+20, 2, 30);
	memset(matrix[4]+20, 0, 30);
	sprintf(matrix[4]+20, "%d %d %d",a[0], a[1], a[2]);
#ifndef __linux__
	CreateThread(NULL, 0, ThreadFunc3, a, 0, NULL);
#else
	pthread_t thread1;
	pthread_attr_t tattr;
	int r;
	r = pthread_attr_init(&tattr);
	r = pthread_attr_setdetachstate(&tattr, PTHREAD_CREATE_DETACHED);
	if(r=pthread_create(&thread1, &tattr, thread_func3, (void*)a)){
		printf("ERROR, rc is %d, so far threads created\n", r);
		perror("Fail:");
		exit(212);
	}
#endif
}
void crash(char** matrix, char** mask, int n, int m, int is, int ie, int j){
	int i;//, j, ie, is;
	memset(mask[5]+20, 2, 30);
	memset(matrix[5]+20, 0, 30);
	sprintf(matrix[5]+20, "%d %d %d",is, ie, j);
	for(i = is; i <= ie; i++){
		if(mask[i%n][j]>=0){
			if((j%2)/*&&((i%n)%2)*/){
				matrix[i%n][j]=0;
				mask[i%n][j] = 0;//rand()%2+1;
				Sleep(70);
			}
		}
	}
}
void render(char**matrix, char** mask, int n, int m){
	int i;
	int j;
#ifndef __linux__
	COORD coordScreen = { 0, 0 };
#else
#endif
	for(i = 0; i < n; i++){
		for(j = 0; j < m; j++){
				if(mask[i][j]>=0&&mask[i][j]<3){
#ifndef __linux__
					coordScreen.X = j;
					coordScreen.Y = i;
					SetConsoleCursorPosition( hStdOut, coordScreen );
#else
					printf("\033[%d;%dH", i, j);
#endif // __linux__
					mask[i][j]+=3;
				switch (mask[i][j]){
					case 3:
						putchar(' ');
						break;
					case 4:
#ifndef __linux__
						SetConsoleTextAttribute(hStdOut, FOREGROUND_GREEN );
#else
						printf("\033[0;32m");
					//	printf("\033[0;92m");
#endif
						putchar(matrix[i][j]);
						break;
					case 5:
#ifndef __linux__
						SetConsoleTextAttribute(hStdOut, FOREGROUND_GREEN | FOREGROUND_INTENSITY);
#else
						printf("\033[0;37m");//92//97
#endif
						putchar(matrix[i][j]);
						break;
				}
				//putchar('\n');
			}
			fflush(stdout);
		}
	}
	//fflush(stdout);
}
